﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum GameState
{
    GS_PAUSEMENU,
    GS_GAME,
    GS_LEVELCOMPLETED,
    GS_GAME_OVER
}

public class GameManager : MonoBehaviour
{
    public GameState currentGameState = GameState.GS_PAUSEMENU;
    public static GameManager instance;
    public Canvas inGameCanvas;
    public Canvas pauseMenuCanvas;
    public Canvas gameOverCanvas;
    public Canvas levelCompletedCanvas;
    public Text starsText;
    private int stars = 0;
    public Text enemiesDeafeated;
    private int enemies = 0;
    public Text gameTimeText;
    public Text gameScoreGameOverText;
    public Text gameScoreLevelCompletedText;
    public float gameTime = 0;
    public Image[] buttonsTab;
    private int buttons = 0;
    public Image[] lifesTab;
    private int lifes = 0;
    private int score = 0;
    public Text highscoreGameOverText;
    public Text LevelCompletedText;
    private int maxSecsToHighscore;
    void Awake()
    {
        instance = this;
        InGame();
        starsText.text = stars.ToString();
        enemiesDeafeated.text = enemies.ToString();
        for (int i = 0; i < buttonsTab.Length; i++)
            buttonsTab[i].color = Color.grey;
        for (int i = 0; i < lifesTab.Length; i++)
            lifesTab[i].enabled = false;
        maxSecsToHighscore = 30;
        if (!PlayerPrefs.HasKey("HighscoreLevel1"))
            PlayerPrefs.SetInt("HighscoreLevel1", 0);
        if (!PlayerPrefs.HasKey("HighscoreLevel2"))
            PlayerPrefs.SetInt("HighscoreLevel2", 0);
    }
    // Use this for initialization
    void Start()
    {
        addLifes();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) && currentGameState == GameState.GS_PAUSEMENU)
        {
            InGame();
        }
        else if (Input.GetKeyDown(KeyCode.Escape) && currentGameState == GameState.GS_GAME)
        {
            PauseMenu();
        }

        if (currentGameState == GameState.GS_GAME) gameTime += Time.deltaTime;
        gameTimeText.text = string.Format("{0:00}:{1:00}", (int)gameTime/60, (int)gameTime%60);
    }
    public void OnResumeButtonClicked()
    {
        InGame();
    }
    public void OnRestartButtonClicked()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void OnExitButtonClicked()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }
    public void OnMenuButtonClicked()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void OnNextButtonClicked()
    {
        SceneManager.LoadScene("scene_2");
    }

    void SetGameState(GameState newGameState)
    {
        currentGameState = newGameState;
        if (newGameState == GameState.GS_LEVELCOMPLETED || newGameState == GameState.GS_GAME_OVER)
        {
            Scene currentScene = SceneManager.GetActiveScene();
            if (currentScene.name == "scene_1")
            {
                score = enemies * 100 + stars * 50 + (lifes + 1) * 200 + (maxSecsToHighscore - (int)gameTime) * 10;
                if (currentGameState == GameState.GS_LEVELCOMPLETED) score += 1000;
                if (PlayerPrefs.GetInt("HighscoreLevel1") < score)
                    PlayerPrefs.SetInt("HighscoreLevel1", score);
                highscoreGameOverText.text = "Highscore: " + PlayerPrefs.GetInt("HighscoreLevel1");
                LevelCompletedText.text = "Highscore: " + PlayerPrefs.GetInt("HighscoreLevel1");
                gameScoreGameOverText.text = string.Format("Your score is: {0}", score);
                gameScoreLevelCompletedText.text = string.Format("Your score is: {0}", score);
            }
            else
            if (currentScene.name == "scene_2")
            {
                score = enemies * 100 + stars * 50 + (lifes + 1) * 200;
                if (currentGameState == GameState.GS_LEVELCOMPLETED) score += 1000;
                if (PlayerPrefs.GetInt("HighscoreLevel2") < score)
                    PlayerPrefs.SetInt("HighscoreLevel2", score);
            }
            highscoreGameOverText.text = "Highscore: " + PlayerPrefs.GetInt("HighscoreLevel2");
            LevelCompletedText.text = "Highscore: " + PlayerPrefs.GetInt("HighscoreLevel2");
            gameScoreGameOverText.text = string.Format("Your score is: {0}", score);
            gameScoreLevelCompletedText.text = string.Format("Your score is: {0}", score);
        }
        inGameCanvas.enabled = (currentGameState == GameState.GS_GAME);
        pauseMenuCanvas.enabled = (currentGameState == GameState.GS_PAUSEMENU);
        gameOverCanvas.enabled = (currentGameState == GameState.GS_GAME_OVER);
        levelCompletedCanvas.enabled = (currentGameState == GameState.GS_LEVELCOMPLETED);

        inGameCanvas.gameObject.SetActive((currentGameState == GameState.GS_GAME));
        pauseMenuCanvas.gameObject.SetActive((currentGameState == GameState.GS_PAUSEMENU));
        gameOverCanvas.gameObject.SetActive((currentGameState == GameState.GS_GAME_OVER));
        levelCompletedCanvas.gameObject.SetActive((currentGameState == GameState.GS_LEVELCOMPLETED));
    }
    public void InGame()
    {
        SetGameState(GameState.GS_GAME);
    }
    public void GameOver()
    {
        SetGameState(GameState.GS_GAME_OVER);
    }
    public void PauseMenu()
    {
        SetGameState(GameState.GS_PAUSEMENU);
    }
    public void LevelCompleted()
    {
        SetGameState(GameState.GS_LEVELCOMPLETED);
    }
    public void addStars()
    {
        stars++;
        starsText.text = stars.ToString();
    }
    public void addEnemy()
    {
        enemies++;
        enemiesDeafeated.text = enemies.ToString();
    }
    public void addButtons()
    {
        buttonsTab[buttons++].color = Color.white;
    }
    public void addLifes()
    {
        lifesTab[++lifes].enabled = true;
    }
    public void subLifes()
    {
        lifesTab[lifes--].enabled = false;
        if (lifes < 0)
        {
            GameOver();
        }
    }
    public void endLevel()
    {
        if (buttons == buttonsTab.Length)
        {
            LevelCompleted();
        }
        else
        {
            Debug.Log("You have to find another " + (buttonsTab.Length - buttons) + " to complete level");
        }
    }
}
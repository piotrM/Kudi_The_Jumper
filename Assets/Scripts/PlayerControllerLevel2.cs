﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource))]

public class PlayerControllerLevel2 : MonoBehaviour
{
    public float moveSpeed = 0.1f;
    public float jumpForce = 6f;
    public int maxKeyNumber = 3;
    private int jumps = 2;

    private bool isWalking = false;
    private bool isGrounded = false;
    private bool isFacingRight = true;

    private float killOffset = 0.1f;
    private Vector3 startPosition;

    private Rigidbody2D rigidBody;
    public LayerMask groundLayer;
    public Animator animator;

    public AudioClip starSound;
    public AudioClip enemyKillSound;
    public AudioClip deathSound;
    public AudioClip winSound;
    public AudioClip lifeSound;
    public AudioClip buttonSound;
    public AudioClip song;
    private AudioSource source;

    // Use this for initialization
    void Start()
    {
        isFacingRight = true;
        isWalking = false;
    }

    //bool isGrounded ()
    //{
    //    return Physics2D.Raycast(this.transform.position, Vector2.down, 0.5f, groundLayer.value);
    //}

    //void Jump ()
    //{
    //    if (isGrounded())
    //    {
    //        rigidBody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
    //    }
    //}

    private void Flip()
    {
        isFacingRight = !isFacingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "Moving Platform")
        {
            transform.parent = other.transform;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Moving Platform")
        {
            transform.parent = null;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    { // avatar is just touching another object
        if (other.CompareTag("Star"))
        {
            GameManager.instance.addStars();
            other.gameObject.SetActive(false);
            source.PlayOneShot(starSound, 1f);
        }
        if (other.CompareTag("Map End"))
        {
            GameManager.instance.subLifes();
            this.transform.position = startPosition;
            source.PlayOneShot(deathSound, 1f);
        }
        if (other.CompareTag("Finish"))
        {
            GameManager.instance.endLevel();
            source.PlayOneShot(winSound, 1f);
        }
        if (other.CompareTag("LifeBonus"))
        {
            GameManager.instance.addLifes();
            other.gameObject.SetActive(false);
            source.PlayOneShot(lifeSound, 1f);
        }
        if (other.CompareTag("Button"))
        {
            GameManager.instance.addButtons();
            other.gameObject.SetActive(false);
            source.PlayOneShot(buttonSound, 1f);
        }
        if (other.CompareTag("Enemy"))
        {
            if (other.gameObject.transform.position.y + killOffset < this.transform.position.y)
            {
                GameManager.instance.addEnemy();
                source.PlayOneShot(enemyKillSound, 1f);
            }
            else
            {
                GameManager.instance.subLifes();
                this.transform.position = startPosition;
                source.PlayOneShot(deathSound, 1f);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.currentGameState == GameState.GS_GAME)
        {
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
            {
                if (!isFacingRight)
                {
                    Flip();
                }
                transform.Translate(moveSpeed * Time.deltaTime, 0, 0, Space.World);
                isWalking = true;
            }
            else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
            {
                if (isFacingRight)
                {
                    Flip();
                }
                transform.Translate(-moveSpeed * Time.deltaTime, 0, 0, Space.World);
                isWalking = true;
            }
            else
            {
                isWalking = false;
            }
            if (
                (Input.GetKeyDown(KeyCode.UpArrow) ||
                Input.GetKeyDown(KeyCode.W) ||
                Input.GetKeyDown(KeyCode.Space)) &&
                jumps > 0)
            {
                rigidBody.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
                jumps--;
            }
        }
        if (rigidBody.velocity.y < 0.001 && rigidBody.velocity.y > -0.001)
        {
            jumps = 2;
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
        animator.SetBool("isGrounded", isGrounded);
        animator.SetBool("isWalking", isWalking);
    }

    void Awake()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        startPosition = transform.position;
        source = GetComponent<AudioSource>();
        source.PlayOneShot(song, 0.1f);
    }
}

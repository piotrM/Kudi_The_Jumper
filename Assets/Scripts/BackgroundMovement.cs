﻿using UnityEngine;
using System.Collections;

public class BackgroundMovement : MonoBehaviour
{
    public float moveSpeed = 0.1f;

    // Use this for initialization
    void Start () {
	
	}
    
    void Update () {

        if (GameManager.instance.currentGameState == GameState.GS_GAME)
        {
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
            {
                transform.Translate(moveSpeed * Time.deltaTime, 0, 0, Space.World);
            }
            else if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
            {
                transform.Translate(-moveSpeed * Time.deltaTime, 0, 0, Space.World);
            }
        }
    }
}

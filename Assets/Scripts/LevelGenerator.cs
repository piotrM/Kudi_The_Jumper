﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour {

    public static LevelGenerator instance;
    public Transform levelStartPoint;

    public List<LevelPieceBasicController> levelPrefabs = new List<LevelPieceBasicController>();
    public List<LevelPieceBasicController> pieces = new List<LevelPieceBasicController>();


    public LevelPieceBasicController startPlatformPrefab;

    public LevelPieceBasicController endPlatformPrefab;

    private bool shouldFinish;
    public float maxGameTime = 5;


    // Use this for initialization
    void Start () {
        instance = this;
        shouldFinish = false;
        ShowPiece((LevelPieceBasicController)Instantiate(startPlatformPrefab));
        AddPiece();
        AddPiece();
        AddPiece();
        AddPiece();
    }

    void Update()
    {
        if (!shouldFinish && GameManager.instance.gameTime > maxGameTime) Finish();
    }
	
	public void AddPiece()
    {
        if (!shouldFinish)
        {
        int randomIndex = Random.Range(0, levelPrefabs.Count - 1);
        LevelPieceBasicController piece = (LevelPieceBasicController)Instantiate (levelPrefabs[randomIndex]);
        piece.transform.SetParent(this.transform, false);

        ShowPiece(piece);
        }
    }

    public void ShowPiece(LevelPieceBasicController piece)
    {
        if (pieces.Count < 1)
            piece.transform.position = new Vector2(
                levelStartPoint.position.x - piece.startPoint.localPosition.x,
                levelStartPoint.position.y - piece.startPoint.localPosition.y);
        else
            piece.transform.position = new Vector2(
                    pieces[pieces.Count - 1].exitPoint.position.x - pieces[pieces.Count - 1].startPoint.localPosition.x,
                    pieces[pieces.Count - 1].exitPoint.position.y - pieces[pieces.Count - 1].startPoint.localPosition.y);

        pieces.Add(piece);
    }

    public void RemoveOldestPiece()
    {
        if(pieces.Count>20)
        {
            LevelPieceBasicController oldestPiece = pieces[0];
            pieces.RemoveAt(0);
            Destroy(oldestPiece.gameObject);
        }
    }

    public void Finish()
    {
        shouldFinish = true;
        ShowPiece((LevelPieceBasicController)Instantiate(endPlatformPrefab));
    }
}

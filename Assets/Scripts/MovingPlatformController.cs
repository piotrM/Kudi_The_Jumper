﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatformController : MonoBehaviour
{
    private bool isMovingRight;
    private bool isFacingRight;
    private bool isDead = false;

    public float XMax = 1;
    public float XMin = 1;
    public float moveSpeed = 0.1f;

    private Collider2D collider;
    public LayerMask groundLayer;

    private float startPositionX;

    // Use this for initialization
    void Start()
    {
        isMovingRight = false;

        collider = GetComponent<Collider2D>();
        startPositionX = transform.position.x;
    }

    void MoveRight()
    {
        transform.Translate(moveSpeed * Time.deltaTime, 0, 0, Space.World);
    }

    void MoveLeft()
    {
        transform.Translate(-moveSpeed * Time.deltaTime, 0, 0, Space.World);
    }

    // Update is called once per frame
    void Update()
    {
        if (isMovingRight && this.transform.position.x < startPositionX + XMax)
            {
                MoveRight();
            }
            else
            {
                isMovingRight = false;
                MoveLeft();
            }
        if (!isMovingRight && this.transform.position.x > startPositionX - XMin)
            {
                MoveLeft();
            }
            else
            {
                isMovingRight = true;
                MoveRight();
            }
    }
}

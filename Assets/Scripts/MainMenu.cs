﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    public Text HighscoreLevel1;
    public Text HighscoreLevel2;

    // Use this for initialization
    void Start ()
    {
        HighscoreLevel1.text = "Highscore: " + PlayerPrefs.GetInt("HighscoreLevel1");
        HighscoreLevel2.text = "Highscore: " + PlayerPrefs.GetInt("HighscoreLevel2");
    }

    void Awake()
    {
        if (!PlayerPrefs.HasKey("HighscoreLevel1"))
            PlayerPrefs.SetInt("HighscoreLevel1", 0);
        if (!PlayerPrefs.HasKey("HighscoreLevel2"))
            PlayerPrefs.SetInt("HighscoreLevel2", 0);
    }
	
	// Update is called once per frame
	void Update () {
    }

    public void onLevel1ButtonPressed()
    {
        StartCoroutine(StartGame("scene_1"));
    }

    public void onLevel2ButtonPressed()
    {
        StartCoroutine(StartGame("scene_2"));
    }


    public void OnExitButtonClicked()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }

    IEnumerator StartGame(string levelName)
    {
        yield return new WaitForSeconds(0.1f);
        SceneManager.LoadScene(levelName);
    }
}

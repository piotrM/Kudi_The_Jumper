﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private bool isMovingRight;
    private bool isFacingRight;
    private bool isDead = false;

    public float XMax = 1;
    public float XMin = 1;
    public float moveSpeed = 0.1f;
    private float killOffset = 0.1f;

    private Collider2D collider1;
    public LayerMask groundLayer;
    public Animator animator;


    private float startPositionX;

    // Use this for initialization
    void Start ()
    {
        isMovingRight = false;
        isFacingRight = false;
        isDead = false;

        collider1 = GetComponent<Collider2D>();

        startPositionX = transform.position.x;

    }

    private void Flip()
    {
        isFacingRight = !isFacingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    void MoveRight()
    {
        if (!isFacingRight)
        {
            Flip();
        }
        transform.Translate(moveSpeed * Time.deltaTime, 0, 0, Space.World);
    }

    void MoveLeft()
    {
        if (isFacingRight)
        {
            Flip();
        }
        transform.Translate(-moveSpeed * Time.deltaTime, 0, 0, Space.World);
    }

    // Update is called once per frame
    void Update()
    {
        if (isMovingRight && !isDead)
        {
            if (this.transform.position.x < startPositionX + XMax)
            {
                MoveRight();
            }
            else
            {
                isMovingRight = false;
                MoveLeft();
                Flip();
            }
        }
        if(!isMovingRight && !isDead)
        {
            if (this.transform.position.x > startPositionX - XMin)
            {
                MoveLeft();
            }
            else
            {
                isMovingRight = true;
                MoveRight();
                Flip();
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            if (other.gameObject.transform.position.y >
            this.transform.position.y + killOffset)
            {
                Debug.Log("Enemy is dead");
                isDead = true;
                animator.SetBool("isDead", isDead);
                collider1.isTrigger = false;
                RemoveSolidity();
                StartCoroutine(KillOnAnimationEnd());
            }
        }
    }

    private IEnumerator KillOnAnimationEnd()
    {
        yield return new WaitForSeconds(0.7f);
        this.gameObject.SetActive(false);
    }

    private void RemoveSolidity()
    {
        var allColliders = GetComponents<Collider2D>();
        var allRigidBodys = GetComponents<Rigidbody2D>();

        foreach (var collider in allColliders) Destroy(collider);
        foreach (var rigidBody in allRigidBodys) Destroy(rigidBody);
    }
}
